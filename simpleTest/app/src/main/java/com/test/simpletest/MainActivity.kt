package com.test.simpletest

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject




class MainActivity : AppCompatActivity() {
    val TAG_LOGS = "app post "
    lateinit var mRecyclerView : RecyclerView
    val mAdapter : RecyclerAdapter = RecyclerAdapter()

    var superheros:MutableList<Post> = ArrayList()
    private var requestQueue: RequestQueue? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        requestQueue = Volley.newRequestQueue(this)


        jsonParse();



    }



    fun jsonParse() {
       val  url = "https://weepatient.com/API/api/Utilidades/Pais_GetPais"
        val postRequest = object : StringRequest(Request.Method.POST, url,
            Response.Listener { response ->
                // response
              //  Log.d("Response", response)
             /*   val toast = Toast.makeText(applicationContext, "Hello Javatpoint"+response, Toast.LENGTH_SHORT)
                toast.show()*/
                val stringResponse = response.toString()
                val jsonObj = JSONObject(stringResponse)
                val jsonArray: JSONObject = jsonObj.getJSONObject("dsRespuesta")
                val paises : JSONArray = jsonArray.getJSONArray("Paises")
                Log.d("Response", paises.toString())
               /// falto agregar parseo a una lista de objetos y desde ahi continuar


                try {
                    for (i in 0 until paises.length()) {
                        val jsonObj = paises.getJSONObject(i)
                        superheros.add(Post(jsonObj.getString("idPais"),  jsonObj.getString("Pais")))
                    }
                    val arrayOfNumbers = superheros
                    val firstValue = arrayOfNumbers!![0].id //
                    Log.d("Response", firstValue)

                    setUpRecyclerView();

                } catch (e: JSONException) {
                    e.printStackTrace()
                }


            },
            Response.ErrorListener {
                // error

                Log.d("Error.Response", "error")
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["cadena"] = ""

                return params
            }
        }
        requestQueue!!.add(postRequest)
    }
    fun setUpRecyclerView(){
        mRecyclerView = findViewById(R.id.rvlist) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mAdapter.RecyclerAdapter((superheros), this)
        mRecyclerView.adapter = mAdapter
    }
}
