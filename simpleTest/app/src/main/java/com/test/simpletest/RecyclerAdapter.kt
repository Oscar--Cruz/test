package com.test.simpletest

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class RecyclerAdapter : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {
    var superheros: MutableList<Post>  = ArrayList()
    lateinit var context: Context
    fun RecyclerAdapter(superheros : MutableList<Post>, context: Context){
        this.superheros = superheros
        this.context = context
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = superheros.get(position)
        holder.bind(item, context)

    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ViewHolder(layoutInflater.inflate(R.layout.item_list, parent, false))
    }
    override fun getItemCount(): Int {
        return superheros.size
    }
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val idPais = view.findViewById(R.id.tvId) as TextView
        val pais = view.findViewById(R.id.tvPais) as TextView
        var imageView=itemView.findViewById(R.id.imageView)as ImageView




        fun bind(superhero:Post, context: Context){
            idPais.text = superhero.id
            pais.text = superhero.pais
            itemView.setOnClickListener(View.OnClickListener {
                //Toast.makeText(context, superhero.id, Toast.LENGTH_SHORT).show()
                imageView.setVisibility(View.VISIBLE);
            })
        }

    }
}